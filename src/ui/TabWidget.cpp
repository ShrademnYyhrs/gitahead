//
//          Copyright (c) 2016, Scientific Toolworks, Inc.
//
// This software is licensed under the MIT License. The LICENSE.md file
// describes the conditions under which this software may be distributed.
//
// Author: Jason Haslam
//

#include "app/Application.h"
#include "dialogs/AccountDialog.h"
#include "dialogs/CloneDialog.h"
#include "host/Account.h"
#include "MenuBar.h"
#include "TabBar.h"
#include "TabWidget.h"
#include "ui/MainWindow.h"
#include "ui/RepoView.h"
#include <QFileDialog>
#include <QFrame>
#include <QHBoxLayout>
#include <QPushButton>
#include <QResizeEvent>
#include <QShortcut>
#include <QVBoxLayout>

namespace
{

	const QString	kLinkFmt = "<a href='%1'>%2</a>";
	const QString	kSupportLink = "mailto:support@gitahead.com";
	const QString	kVideoLink = "https://gitahead.com/#tutorials";

	class DefaultWidget: public QFrame
	{
public:
		DefaultWidget(QWidget *parent = nullptr): QFrame(parent)
		{
			QPushButton *clone(new QPushButton(tr("Clone repository"), this));
			connect(clone, &QPushButton::clicked, [this]
			{
				CloneDialog *dialog = new CloneDialog(CloneDialog::Clone, this);
				connect(dialog, &CloneDialog::accepted, [dialog]
				{
					if (MainWindow *window = MainWindow::open(dialog->path()))
						window->currentView()->addLogEntry(
							dialog->message(), dialog->messageTitle());
				});
				dialog->open();
			});

			QPushButton *open(new QPushButton(tr("Open existing repository"), this));
			connect(open, &QPushButton::clicked, [this]
			{
				// FIXME: Filter out non-git dirs.
				QFileDialog *dialog =
					new QFileDialog(this, tr("Open Repository"), QDir::homePath());
				dialog->setAttribute(Qt::WA_DeleteOnClose);
				dialog->setFileMode(QFileDialog::Directory);
				dialog->setOption(QFileDialog::ShowDirsOnly);
				connect(dialog, &QFileDialog::fileSelected, [](const QString &path)
				{
					MainWindow::open(path);
				});
				dialog->open();
			});

			QPushButton *init(new QPushButton(tr("Initialize new repository"), this));
			connect(init, &QPushButton::clicked, [this]
			{
				CloneDialog *dialog = new CloneDialog(CloneDialog::Init, this);
				connect(dialog, &CloneDialog::accepted, [dialog]
				{
					if (MainWindow *window = MainWindow::open(dialog->path()))
						window->currentView()->addLogEntry(
							dialog->message(), dialog->messageTitle());
				});
				dialog->open();
			});

			QVBoxLayout *layout = new QVBoxLayout(this);
			layout->setSpacing(12);
			layout->addWidget(clone);
			layout->addWidget(open);
			layout->addWidget(init);
			layout->addWidget(addSeparator());

			for (int i = 0; i < Account::KindNumber; ++i)
			{
				Account::Kind	kind(static_cast<Account::Kind>(i));
				QPushButton		*account(new QPushButton(tr("Add %1 account").arg(Account::name(kind)), this));

				connect(account, &QPushButton::clicked, [this, kind]
				{
					AccountDialog *dialog = new AccountDialog(nullptr, this);
					dialog->setKind(kind);
					dialog->open();
				});
				layout->addWidget(account);
			}

			layout->addWidget(addSeparator());
			layout->addWidget(addLink(tr("View getting started videos"), kVideoLink));
			layout->addWidget(addLink(tr("Contact us for support"), kSupportLink));
		}

private:
		QLabel *addLink(const QString &text, const QString &link = QString())
		{
			QLabel *label = new QLabel(kLinkFmt.arg(link, text), this);
			label->setOpenExternalLinks(true);

			QFont font = label->font();
			font.setPointSize(font.pointSize() + 3);
			label->setFont(font);

			return label;
		}

		QFrame *addSeparator()
		{
			QFrame *separator = new QFrame(this);
			separator->setStyleSheet("border: 2px solid palette(dark)");
			separator->setFrameShape(QFrame::HLine);
			return separator;
		}
	};

} // anon. namespace

TabWidget::TabWidget(QWidget *parent): QTabWidget{parent}
{
	auto	*bar{new TabBar(this)};
	auto	*close{new QShortcut{Qt::CTRL + Qt::Key_W, this}};

	bar->setMovable(true);
	bar->setTabsClosable(true);
	setTabBar(bar);

	// Create default widget.
	mDefaultWidget = new DefaultWidget(this);

	connect(close, &QShortcut::activated, [this]
	{
		emit tabCloseRequested(currentIndex());
	});
	connect(this, &TabWidget::tabBarDoubleClicked, this, &TabWidget::detachTab);
	connect(this, &TabWidget::tabCloseRequested, [this](int index)
	{
		emit tabAboutToBeRemoved();
		widget(index)->close();
	});
}

void TabWidget::detachTab(int index)
{
	auto	*page{widget(index)};
	auto	*dialog{new QDialog{this, Qt::Window}};
	auto	*layout{new QHBoxLayout};

	dialog->setWindowTitle(tabBar()->tabText(index));
	layout->addWidget(page);
	dialog->setLayout(layout);
	dialog->resize(page->width(), page->height());
	dialog->show();
	page->setVisible(true);
	connect(dialog, &QDialog::finished, [this, page, dialog]
	{
		insertTab(0, page, dialog->windowTitle());
		setCurrentIndex(0);
	});
}

void TabWidget::resizeEvent(QResizeEvent *event)
{
	QTabWidget::resizeEvent(event);

	QSize	size = event->size();
	QSize	sizeHint = mDefaultWidget->sizeHint();
	int		x = (size.width() - sizeHint.width()) / 2;
	int		y = (size.height() - sizeHint.height()) / 2;
	mDefaultWidget->move(x, y);
}

void TabWidget::tabInserted(int index)
{
	QTabWidget::tabInserted(index);
	MenuBar::instance(this)->updateWindow();
	emit tabInserted();

	mDefaultWidget->setVisible(false);
}

void TabWidget::tabRemoved(int index)
{
	QTabWidget::tabRemoved(index);
	MenuBar::instance(this)->updateWindow();
	emit tabRemoved();

	mDefaultWidget->setVisible(!count());
}
